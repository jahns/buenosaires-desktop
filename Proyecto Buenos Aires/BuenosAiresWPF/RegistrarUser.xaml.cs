﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using BuenosAires;
using System.Text.RegularExpressions;

namespace BuenosAiresWPF
{
    /// <summary>
    /// Lógica de interacción para RegistrarUser.xaml
    /// </summary>
    public partial class RegistrarUser : Page
    {
        public RegistrarUser()
        {
            InitializeComponent();
        }

        private void btnAtras_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.GoBack();
        }

        private void btnRegistrarUser_Click(object sender, RoutedEventArgs e)
        {
            if (ValidarDatos())
            {
                User user = new User()
                {
                    First_name = txtFirstName.Text,
                    Last_name = txtLastName.Text,
                    Email = txtEmail.Text,
                    Username = txtUsername.Text,
                    Password = pwPassword.Password.ToString()
                };

                if (user.Create())
                {
                    Message("Registro de usuario", "Usuario creado!");
                    LimpiarCampos();
                }
                else
                {
                    Message("Registro de usuario", "Usuario no creado", true);
                }
            }
        }

        private bool ValidarDatos()
        {
            bool validar = false;

            if (
                    txtFirstName.Text != "" &&
                    txtLastName.Text != "" &&
                    txtEmail.Text != "" &&
                    txtUsername.Text != "" &&
                    pwPassword.Password.ToString() != "" &&
                    pwCoPassword.Password.ToString() != ""
                )
            {
                Regex emailRx = new Regex(@"^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$");

                if (emailRx.IsMatch(txtEmail.Text))
                {
                    if (pwPassword.Password.ToString() == pwCoPassword.Password.ToString())
                    {
                        validar = true;
                    }
                    else
                    {
                        Message("Error en contraseñas", "Las contraseñas no son iguales", true);
                    }
                }
                else
                {
                    Message("Error en email", "Ingrese un email valido", true);
                }
            }
            else
            {
                Message("Error en datos", "No debe haber campos vacios", true);
            }

            return validar;
        }

        private void LimpiarCampos()
        {
            txtFirstName.Clear();
            txtLastName.Clear();
            txtEmail.Clear();
            txtUsername.Clear();
            pwPassword.Clear();
            pwCoPassword.Clear();
        }

        private void Message(string title, string message, bool error = false)
        {
            if (!error)
            {
                MessageBox.Show(message, title, MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {
                MessageBox.Show(message, title, MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
