﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using BuenosAires;

namespace BuenosAiresWPF
{
    /// <summary>
    /// Lógica de interacción para Stock.xaml
    /// </summary>
    public partial class Stock : Page
    {
        public Stock()
        {
            InitializeComponent();
            ListarProductos();
        }

        private void ListarProductos()
        {
            Producto producto = new Producto();
            List<dynamic> productos = new List<dynamic>();

            foreach (var p in producto.ListarProductos())
            {
                var objP = new
                {
                    Id = p.Id,
                    Imagesrc = p.Imagesrc,
                    Descripcion = p.Descripcion,
                    Modelo = p.Modelo,
                    Precio = p.Precio,
                    Cantidad = p.Cantidad,
                    Tipo = p.Tipo,
                    Marca = p.Marca.Descripcion,
                    Proveedor = p.Proveedor.Nombre
                };
                productos.Add(objP);
            }

            dgProductos.ItemsSource = productos;
        }

        private void btnAtras_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.GoBack();
        }
    }
}
