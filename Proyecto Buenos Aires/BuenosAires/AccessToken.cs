﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace BuenosAires
{
    public class AccessToken
    {
        //variables
        public string token { get; set; }

        //credenciales buenosaires-desktop
        string client_id = "ArBAXqzCo6zxKwNBw0ZhYvbYGWWl91OFoLahg2Mn";
        string client_secret = "y5ZXEfSxTfiiq83lHXtmpjYiBQI1ApGIHDNONNKCYsAWRLe5sWLwJ17DZwwwdQpt0nJyOpjZItVymWcBfLC7CyNhd0ri46Mv9ZpjrghTMU2FnWrWoBLMspXKRzALdflb";

        //builder
        public AccessToken()
        {
            this.init();
        }

        private void init()
        {
            token = string.Empty;
            ObtenerAccessToken();
        }

        //metodo para obtener token
        private string ObtenerAccessToken()
        {
            try
            {
                string readerString;
                JObject readerJson;
                string token = "";

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://localhost:9000/oauth/token/");
                request.Method = "POST";
                string postData = "grant_type=client_credentials&client_id=" + this.client_id + "&client_secret=" + this.client_secret;
                ASCIIEncoding encoding = new ASCIIEncoding();
                byte[] byteArray = encoding.GetBytes(postData);

                request.ContentType = "application/x-www-form-urlencoded";

                request.ContentLength = byteArray.Length;
                Stream newStream = request.GetRequestStream();
                newStream.Write(byteArray, 0, byteArray.Length);

                HttpWebResponse response = request.GetResponse() as HttpWebResponse;
                using (Stream responseStream = response.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);
                    readerString = reader.ReadToEnd();

                    readerJson = JObject.Parse(readerString);
                    foreach (var jsonItem in readerJson)
                    {
                        if (jsonItem.Key.Equals("access_token"))
                        {
                            //Console.WriteLine(jsonItem.Value);
                            token = jsonItem.Value.ToString();
                        }
                    }
                }

                this.token = token;
                return token;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
