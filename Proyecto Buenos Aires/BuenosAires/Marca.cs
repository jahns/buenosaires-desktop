﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace BuenosAires
{
    public class Marca
    {
        //variables
        public string Url { get; set; }
        public int Id { get; set; }
        public string Descripcion { get; set; }

        //builder
        public Marca()
        {
            this.init();
        }

        private void init()
        {
            Url = string.Empty;
            Id = 0;
            Descripcion = string.Empty;
        }

        //metodo para buscar marca
        public Marca BuscarMarca(string url)
        {
            try
            {
                Marca marca = new Marca();
                AccessToken access = new AccessToken();
                string readerString;
                JArray readerJson;
                List<JObject> jsonObjects = new List<JObject>();

                var uri = new Uri("http://localhost:9000/api/marca/");
                var webRequest = WebRequest.Create(uri);
                var request = (HttpWebRequest)webRequest;
                request.PreAuthenticate = true;
                request.Headers.Add("Authorization", "Bearer " + access.token);
                request.Accept = "application/json";

                var response = request.GetResponse();
                using (Stream responseStream = response.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);
                    readerString = reader.ReadToEnd();

                    //Console.WriteLine(readerString);
                    readerJson = JArray.Parse(readerString);

                    foreach (var jsonItem in readerJson.Children<JObject>())
                    {
                        if (jsonItem["url"].ToString().Equals(url))
                        {
                            marca = ProcessJson(jsonItem);
                        }
                    }
                }

                return marca;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return null;
            }
        }

        //metodo para procesar objeto json
        private Marca ProcessJson(JObject jsonObj)
        {
            Marca marca = new Marca()
            {
                Url = jsonObj["url"].ToString(),
                Id = Int32.Parse(jsonObj["id"].ToString()),
                Descripcion = jsonObj["descripcion"].ToString()
            };

            return marca;
        }
    }
}
