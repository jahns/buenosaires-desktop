﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace BuenosAires
{
    public class Proveedor
    {
        //variables
        public string Url { get; set; }
        public int Id { get; set; }
        public string Nombre { get; set; }
        public int Telefono { get; set; }
        public string Direccion { get; set; }
        public string PaginaWeb { get; set; }

        //builder
        public Proveedor()
        {
            this.init();
        }

        private void init()
        {
            Url = string.Empty;
            Id = 0;
            Nombre = string.Empty;
            Telefono = 0;
            Direccion = string.Empty;
            PaginaWeb = string.Empty;
        }

        //metodo para buscar proveedor
        public Proveedor BuscarProveedor(string url)
        {
            try
            {
                Proveedor proveedor = new Proveedor();
                AccessToken access = new AccessToken();
                string readerString;
                JArray readerJson;
                List<JObject> jsonObjects = new List<JObject>();

                var uri = new Uri("http://localhost:9000/api/proveedor/");
                var webRequest = WebRequest.Create(uri);
                var request = (HttpWebRequest)webRequest;
                request.PreAuthenticate = true;
                request.Headers.Add("Authorization", "Bearer " + access.token);
                request.Accept = "application/json";

                var response = request.GetResponse();
                using (Stream responseStream = response.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);
                    readerString = reader.ReadToEnd();

                    //Console.WriteLine(readerString);
                    readerJson = JArray.Parse(readerString);

                    foreach (var jsonItem in readerJson.Children<JObject>())
                    {
                        if (jsonItem["url"].ToString().Equals(url))
                        {
                            proveedor = ProcessJson(jsonItem);
                        }
                    }
                }

                return proveedor;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return null;
            }
        }

        //metodo para procesar objeto json
        private Proveedor ProcessJson(JObject jsonObj)
        {
            Proveedor proveedor = new Proveedor()
            {
                Url = jsonObj["url"].ToString(),
                Id = Int32.Parse(jsonObj["id"].ToString()),
                Nombre = jsonObj["nombre"].ToString(),
                Telefono = Int32.Parse(jsonObj["telefono"].ToString()),
                Direccion = jsonObj["direccion"].ToString(),
                PaginaWeb = jsonObj["pagina_web"].ToString()
        };

            return proveedor;
        }
    }
}
