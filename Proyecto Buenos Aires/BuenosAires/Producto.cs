﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace BuenosAires
{
    public class Producto
    {
        //variables
        public string Url { get; set; }
        public int Id { get; set; }
        public string Imagesrc { get; set; }
        public string Descripcion { get; set; }
        public string Modelo { get; set; }
        public int Precio { get; set; }
        public int Cantidad { get; set; }
        public string Tipo { get; set; }
        public Marca Marca { get; set; }
        public Proveedor Proveedor { get; set; }

        //builder
        public Producto()
        {
            this.init();
        }

        private void init()
        {
            Url = string.Empty;
            Id = 0;
            Imagesrc = string.Empty;
            Descripcion = string.Empty;
            Modelo = string.Empty;
            Precio = 0;
            Cantidad = 0;
            Tipo = string.Empty;
            Marca = null;
            Proveedor = null;
        }

        //metodo para listar productos
        public List<Producto> ListarProductos()
        {
            try
            {
                List<Producto> productos = new List<Producto>();
                AccessToken access = new AccessToken();
                string readerString;
                JArray readerJson;
                List<JObject> jsonObjects = new List<JObject>();

                var uri = new Uri("http://localhost:9000/api/producto/");
                var webRequest = WebRequest.Create(uri);
                var request = (HttpWebRequest)webRequest;

                request.PreAuthenticate = true;
                request.Headers.Add("Authorization", "Bearer " + access.token);
                request.Accept = "application/json";

                var response = request.GetResponse();
                using (Stream responseStream = response.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);
                    readerString = reader.ReadToEnd();

                    //Console.WriteLine(readerString);
                    readerJson = JArray.Parse(readerString);

                    foreach (var jsonItem in readerJson.Children<JObject>())
                    {
                        productos.Add(ProcessJson(jsonItem));
                    }
                }

                return productos;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return null;
            }
        }

        //metodo para procesar objeto json
        private Producto ProcessJson(JObject jsonObj)
        {
            Marca marca = new Marca();
            Proveedor proveedor = new Proveedor();
            Producto producto = new Producto()
            {
                Url = jsonObj["url"].ToString(),
                Id = Int32.Parse(jsonObj["id"].ToString()),
                Imagesrc = jsonObj["imagesrc"].ToString(),
                Descripcion = jsonObj["descripcion"].ToString(),
                Modelo = jsonObj["modelo"].ToString(),
                Precio = Int32.Parse(jsonObj["precio"].ToString()),
                Cantidad = Int32.Parse(jsonObj["cantidad"].ToString()),
                Tipo = jsonObj["tipo"].ToString(),
                Marca = marca.BuscarMarca(jsonObj["marca"].ToString()),
                Proveedor = proveedor.BuscarProveedor(jsonObj["proveedor"].ToString()),
            };

            return producto;
        }
    }
}
