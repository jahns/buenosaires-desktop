﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace BuenosAires
{
    public class User
    {
        //variables
        public string Url { get; set; }
        public int Id { get; set; }
        public string First_name { get; set; }
        public string Last_name { get; set; }
        public string Email { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }

        //builder
        public User()
        {
            this.init();
        }

        private void init()
        {
            Url = string.Empty;
            Id = 0;
            First_name = string.Empty;
            Last_name = string.Empty;
            Email = string.Empty;
            Username = string.Empty;
            Password = string.Empty;
        }

        //metodo para agregar usuario
        public bool Create()
        {
            try
            {
                string readerString;
                JObject readerJson;

                WebRequest request = WebRequest.Create("http://localhost:8000/api/user/");

                string postData = "first_name=" + this.First_name + "&";
                postData += "last_name=" + this.Last_name + "&";
                postData += "email=" + this.Email + "&";
                postData += "username=" + this.Username + "&";
                postData += "password=" + this.Password;

                byte[] byteArray = Encoding.UTF8.GetBytes(postData);

                request.PreAuthenticate = false;
                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";
                request.ContentLength = byteArray.Length;

                using (Stream newStream = request.GetRequestStream())
                {  
                    newStream.Write(byteArray, 0, byteArray.Length);
                    WebResponse response = request.GetResponse();

                    using (Stream responseStream = response.GetResponseStream())
                    {
                        StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);
                        readerString = reader.ReadToEnd();

                        readerJson = JObject.Parse(readerString);
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return false;
            }
        }
    }
}
